groups:
- name: KafkaRules
  rules:
    - alert: KafkaUnderReplicatedPartitions
      expr: kafka_server_replicamanager_underreplicatedpartitions > 0
      for: 10s
      labels:
        severity: warning
        group: kafka
      annotations:
        summary: 'Kafka under replicated partitions'
        description: 'There are {{ $value }} under replicated partitions on {{ $labels.instance }}'
    - alert: KafkaAbnormalControllerState
      expr: sum(kafka_controller_kafkacontroller_activecontrollercount) != 1
      for: 10s
      labels:
        severity: warning
        group: kafka
      annotations:
        summary: 'Kafka abnormal controller state'
        description: 'There are {{ $value }} active controllers in the cluster'

    - alert: KafkaOfflinePartitions
      expr: sum(kafka_controller_kafkacontroller_offlinepartitionscount) > 0
      for: 10s
      labels:
        severity: warning
        group: kafka
      annotations:
        summary: 'Kafka offline partitions'
        description: '{{ $value }} partitions have no leader'

    - alert: KafkaUnderMinIsrPartitionCount
      expr: kafka_server_replicamanager_underminisrpartitioncount > 0
      for: 10s
      labels:
        severity: warning
        group: kafka
      annotations:
        summary: 'Kafka under min ISR partitions'
        description: 'There are {{ $value }} partitions under the min ISR on {{ $labels.instance }}'

    - alert: KafkaOfflineLogDirectoryCount
      expr: kafka_log_logmanager_offlinelogdirectorycount > 0
      for: 10s
      labels:
        severity: warning
        group: kafka
      annotations:
        summary: 'Kafka offline log directories'
        description: 'There are {{ $value }} offline log directories on {{ $labels.instance }}'

    - alert: BrokerOverLoaded
      expr: avg(sum by(instance) (rate(kafka_network_requestmetrics_requests_total[15m]))) > 1000
      for: 5m
      labels:
        severity: critical
        group: kafka
      annotations:
        description: 'broker {{ $labels.instance }} overloaded (current value is: {{ $value }})'
        summary: 'broker overloaded'
        command: 'upScale'
    - alert: PartitionCountHigh
      expr: max(kafka_server_replicamanager_partitioncount)  by (instance) > 800
      for: 3m
      labels:
        severity: critical
        group: kafka
      annotations:
        description: 'broker {{ $labels.instance }} has high partition count'
        summary: 'high partition count'
        command: 'upScale'
    - alert: PartitionCountLow
      expr: min(kafka_server_replicamanager_partitioncount)  by (instance) < 40
      for: 3m
      labels:
        severity: critical
        group: kafka
      annotations:
        description: 'broker {{ $labels.instance }} has low partition count'
        summary: 'low partition count'
        command: 'downScale'
